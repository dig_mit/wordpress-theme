    <div class="footer-background-extension"></div>
    <footer id="site-footer">
      <div class="container">
        <?php get_sidebar( 'footer' ); ?>

	<div class="powered-by">
          <div>
            <a href="https://digmit.at/index.php/impressum-datenschutz/">Impressum</a> | <a href="https://digmit.at/index.php/datenschutz/">Datenschutz</a> | </div>
          <div>
            Web development by <a href="https://tantemalkah.at">Tante Malkah</a> |
          </div>
          <div>
            Powered by <a href="https://wordpress.org">WordPress</a>
          </div>
        </div>
        <button id="back-to-top-button" title="Hinauf zum Seitenanfang">
          <span class="dashicons dashicons-arrow-up-alt"></span>
        </button>
      </div>
      <?php wp_footer(); ?>
    </footer>
  </body>
</html>

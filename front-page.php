<?php
get_header();
?>

<main<?= is_front_page() ? ' class="front-page"' : ''; ?>>
  <div class="container">
    <?php
    // if there is no front page set, WP will automagically use a different
    // template, so no need for checking if there is a post
    the_post();

    the_content();
    ?>
  </div>
</main>

<?php
get_footer();

<?php

function digmit_register_styles () {
  wp_enqueue_style( 'digmit_style', get_stylesheet_uri() );
  wp_enqueue_style( 'dashicons' );
  wp_enqueue_script(
    'back-to-top-button-script',
    get_template_directory_uri() . '/assets/js/back-to-top.js',
    array(), // no dependecies
    null, // no version string
    true // load in footer
  );
}
add_action( 'wp_enqueue_scripts', 'digmit_register_styles' );

function digmit_register_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Hauptmenü' ),
      'extra-menu' => __( 'Zusatzmenü' )
     )
   );
 }
 add_action( 'init', 'digmit_register_menus' );


function digmit_register_sidebars() {
   register_sidebar(
     array(
       'id'            => 'footer',
       'name'          => __( 'Footer Widget Area' ),
       'description'   => __( 'Widgets placed here will appear in the footer of the page.' ),
     )
   );
}
add_action( 'widgets_init', 'digmit_register_sidebars' );

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <title><?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>

  </head>

  <body>

    <header<?= is_front_page() ? ' class="front-page"' : ''; ?>>
      <!-- for screen widths smaller than 640px -->
      <div class="container narrow-header">
        <!-- left header column for logo -->
        <div id="logo">
          <a href="<?= site_url(); ?>">
            <img src="<?= get_theme_file_uri('assets/img/digmit_logo.png'); ?>">
          </a>
        </div>
        <!-- right header columns for header content -->
        <div>
          <nav id="site-nav">
            <div>
              <a href="<?= site_url(); ?>"><span class="dashicons dashicons-admin-home"></span></a>
              <a href="#site-footer"><span class="dashicons dashicons-businesswoman"></span></a>
              <label for="search-form-checkbox"><span class="dashicons dashicons-search"></span></label>
              <label for="main-nav-checkbox"><span class="dashicons dashicons-menu"></span></label>
            </div>
          </nav>
          <div id="site-title">
            <h1>
              <a href="<?= site_url(); ?>">
                d<span class="dashicons dashicons-info"></span>g_m<span class="dashicons dashicons-info"></span>t!
              </a>
            </h1>
          </div>
        </div>
        <div>
          <nav id="highlights-nav">
            <?php
            wp_nav_menu([
              'theme_location' => 'extra-menu',
              'container_id' => 'highlights-nav-items'
            ]);
            ?>
          </nav>
          <nav id="main-nav">
            <input type="checkbox" id="main-nav-checkbox"/>
            <?php
            wp_nav_menu([
              'theme_location' => 'main-menu',
              'container_id' => 'main-nav-items'
            ]);
            ?>
          </nav>
          <div id="search-form">
            <input type="checkbox" id="search-form-checkbox"/>
            <?php get_search_form(); ?>
          </div>
        </div>
      </div>

      <!-- for screen widths broader than 840px -->
      <div class="container wide-header">
        <div>
          <!-- left header column with logo and site title -->
          <div id="logo">
            <a href="<?= site_url(); ?>">
              <img src="<?= get_theme_file_uri('assets/img/digmit_logo.png'); ?>">
            </a>
          </div>
          <div id="site-title">
            <h1>
              <a href="<?= site_url(); ?>">
                d<span class="dashicons dashicons-info"></span>g_m<span class="dashicons dashicons-info"></span>t!
              </a>
            </h1>
          </div>
        </div>
        <!-- right header column with header content -->
        <div>
          <!-- highlights menu -->
          <!--
          <div>
            <nav id="highlights-nav">
              <?php
              /*
              wp_nav_menu([
                'theme_location' => 'extra-menu',
                'container_id' => 'highlights-nav-items'
              ]);
              */
              ?>
            </nav>
          </div>
          -->
          <!-- main menu -->
          <nav id="main-nav">
            <?php
            wp_nav_menu([
              'theme_location' => 'main-menu',
              'container_id' => 'main-nav-items'
            ]);
            ?>
          </nav>
          <!-- site navigation with home, contacts and search button -->
          <nav id="site-nav">
            <div>
              <a href="<?= site_url(); ?>"><span class="dashicons dashicons-admin-home"></span></a>
              <a href="#site-footer"><span class="dashicons dashicons-businesswoman"></span></a>
              <label for="search-form-checkbox-wide"><span class="dashicons dashicons-search"></span></label>
            </div>
          </nav>
        </div>
      </div>
      <div class="container wide-header">
        <div id="search-form">
          <input type="checkbox" id="search-form-checkbox-wide"/>
          <?php get_search_form(); ?>
        </div>
      </div>
    </header>
    <div class="header-background-extension"></div>

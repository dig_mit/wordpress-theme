<?php
get_header();
?>

<main>
  <div class="container">
    <?php
    if ( have_posts() ) {

      $i = 0;

      while ( have_posts() ) {
        $i++;
        if ( $i > 1 ) {
          echo '<hr class="post-separator styled-separator is-style-wide section-inner" aria-hidden="true" />';
        }
        the_post();

        ?> <article> <?php
        the_title( '<h1 class="entry-title">', '</h1>' );
        the_content();
        ?> </article> <?php

      }
    } elseif ( is_search() ) {
      ?>

      <div class="no-search-results-form section-inner thin">

        <h1><?php _e("Keine Suchergebnisse gefunden", "digmit"); ?></h1>
        <p>
          <?php
          _e("Es konnten keine Suchergebnisse gefunden werden. Probiere es mit einem anderen Begriff:", "digmit");
          get_search_form();
          ?>
        </p>

      </div><!-- .no-search-results -->

      <?php
    }
    ?>
  </div>
</main>

<?php
get_footer();

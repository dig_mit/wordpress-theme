backToTopButton = document.getElementById("back-to-top-button");

backToTopButton.addEventListener( "click", function () {
  document.documentElement.scrollTop = 0;
  document.body.scrollTop = 0; // Safari needs this
});

// when user is scrolling, check how much and display button accordingly
window.onscroll = function() {
  if ( document.body.scrollTop > 180 || document.documentElement.scrollTop > 180 ) {
    backToTopButton.style.display = "block";
  } else {
    backToTopButton.style.display = "none";
  }
};
